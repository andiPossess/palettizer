﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Path = System.IO.Path;

using Microsoft.Win32;
using System.Globalization;

namespace Palettizer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        public MainWindow()
        {
            InitializeComponent();

            Palettes = new ObservableCollection<List<PaletteColor>>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private string lastFile = null;
        private Dictionary<Color, Color> paletteDictionary = new Dictionary<Color, Color>();

        public BitmapSource currentImage;

        public ObservableCollection<List<PaletteColor>> Palettes { get; set; }
        public string Warning { get; set; }

        private ImageSource CreateTiles(BitmapSource sourceImage)
        {
            if (sourceImage == null) return null;

            int tileWidth, tileHeight, sourceMargin, sourceSpacing, targetMargin, targetSpacing, bleed;

            if (!int.TryParse(SourceTileWidth.Text, out tileWidth)) return null;
            if (!int.TryParse(SourceTileHeight.Text, out tileHeight)) return null;
            if (!int.TryParse(SourceMargin.Text, out sourceMargin)) return null;
            if (!int.TryParse(SourceSpacing.Text, out sourceSpacing)) return null;
            if (!int.TryParse(TargetMargin.Text, out targetMargin)) return null;
            if (!int.TryParse(TargetSpacing.Text, out targetSpacing)) return null;
            if (!int.TryParse(TargetBleed.Text, out bleed)) return null;

            if (tileHeight + sourceMargin * 2 > sourceImage.PixelHeight) return null;
            if (tileWidth + sourceMargin * 2 > sourceImage.PixelWidth) return null;

            if (tileWidth == 0) return null;
            if (tileHeight == 0) return null;

            int tilesWide = (sourceImage.PixelWidth - 2 * sourceMargin + sourceSpacing) / (tileWidth + sourceSpacing);
            int tilesTall = (sourceImage.PixelHeight - 2 * sourceMargin + sourceSpacing) / (tileHeight + sourceSpacing);

            WriteableBitmap bitmap = new WriteableBitmap(
                tilesWide * (tileWidth + targetSpacing + bleed * 2) - targetSpacing + targetMargin * 2,
                tilesTall * (tileHeight + targetSpacing + bleed * 2) - targetSpacing + targetMargin * 2,
                sourceImage.DpiX, sourceImage.DpiY, sourceImage.Format, null
                );


            int tileStride = tileWidth * (currentImage.Format.BitsPerPixel) / 8;
            int bleedStride = bleed * 2 * (currentImage.Format.BitsPerPixel) / 8;

            for (int i = 0; i < tilesWide; i++)
            {
                for (int j = 0; j < tilesTall; j++)
                {
                    Int32Rect sourceRect = new Int32Rect(sourceMargin + (tileWidth + sourceSpacing) * i,
                                                   sourceMargin + (tileHeight + sourceSpacing) * j,
                                                   tileWidth, tileHeight);

                    Int32Rect targetRect = new Int32Rect(targetMargin + (tileWidth + targetSpacing + bleed * 2) * i,
                                                        targetMargin + (tileHeight + targetSpacing + bleed * 2) * j,
                                                        tileWidth + bleed * 2, tileHeight + bleed * 2);

                    bitmap.WritePixels(sourceImage.CopyPixels(sourceRect).CreateBleed(bleed), targetRect);
                }
            }

            return bitmap;
        }

        private ImageSource IndexByPalette(BitmapSource sourceImage, Dictionary<Color, Color> palette)
        {
            int stride = currentImage.PixelWidth * (currentImage.Format.BitsPerPixel / 8);
            byte[] pixels = new byte[currentImage.PixelHeight * stride];
            currentImage.CopyPixels(pixels, stride, 0);

            for (int i = 0; i < pixels.Length; i += 4)
            {
                var c = new Color();
                c.A = Byte.MaxValue;
                c.R = pixels[i + 2];
                c.G = pixels[i + 1];
                c.B = pixels[i + 0];

                Color newColor;
                if (palette.ContainsKey(c))
                {
                    newColor = palette[c];
                }
                else
                {
                    newColor = new Color() { A = 0, R = 0, G = 0, B = 0 };
                }

                pixels[i + 2] = newColor.R;
                pixels[i + 1] = newColor.G;
                pixels[i + 0] = newColor.B;
            }

            BitmapSource image = BitmapSource.Create(
            currentImage.PixelWidth, currentImage.PixelHeight,
            currentImage.DpiX, currentImage.DpiY,
            currentImage.Format, null, pixels, stride);

            return image;
        }

        private void OnLoadImageButtonClick(object sender, RoutedEventArgs e)
        {
            currentImage = BitmapHelper.LoadPng(ref lastFile) ?? currentImage;
            (SourceImage.Template.FindName("Image", SourceImage) as Image).Source = currentImage;
        }

        private void OnUpdatePreviewClick(object sender, RoutedEventArgs e)
        {
            ImageSource source = null;

            switch ((ToolTabs.SelectedItem as TabItem)?.Name)
            {
                case "BleedTab":
                    source = CreateTiles(currentImage);
                    break;

                case "PaletteTab":
                    source = Palettize(currentImage);
                    break;
            }

            (PreviewImage.Template.FindName("Image", PreviewImage) as Image).Source = source;
        }

        private void OnGetPaletteButtonClick(object sender, RoutedEventArgs e)
        {
            if (currentImage == null) return;

            int stride = currentImage.PixelWidth * (currentImage.Format.BitsPerPixel / 8);
            byte[] pixels = new byte[currentImage.PixelHeight * stride];
            currentImage.CopyPixels(pixels, stride, 0);

            Dictionary<Color, int> dict = new Dictionary<Color, int>();

            for (int i = 0; i < pixels.Length; i += 4)
            {
                var c = new Color();
                c.A = Byte.MaxValue;
                c.R = pixels[i + 2];
                c.G = pixels[i + 1];
                c.B = pixels[i + 0];

                if (dict.ContainsKey(c)) dict[c]++;
                else dict[c] = 1;
            }

            Palettes.Clear();
            var l = new List<PaletteColor>();
            Style rectStyle = FindResource("PaletteRect") as Style;
            foreach (var pair in dict)
            {
                l.Add(new PaletteColor(pair.Key));
            }

            Palettes.Add(l);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Palettes"));
        }

        private List<Color> RetrievePalette(BitmapSource source, int swatchWidth, int swatchHeight, int numberOfColors)
        {
            var colors = source.CopyPixels();
            var palette = new List<Color>();

            for (int j = 0; j < colors.GetLength(1); j += swatchHeight)
            {
                for (int i = 0; i < colors.GetLength(0); i += swatchWidth)
                {
                    Color c = colors[i, j];
                    c.A = 255;
                    palette.Add(c);
                    if (palette.Count == numberOfColors) return palette;
                }
            }
            return palette;
        }

        private void OnImportAsPaletteClick(object sender, RoutedEventArgs e)
        {
            if (currentImage == null) return;

            int swatchWidth, swatchHeight, numberOfColors;
            if (!int.TryParse(ImportSwatchWidth.Text, out swatchWidth)) return;
            if (!int.TryParse(ImportSwatchHeight.Text, out swatchHeight)) return;
            if (!int.TryParse(ImportPaletteLength.Text, out numberOfColors)) return;

            if (swatchWidth == 0 || swatchHeight == 0 || numberOfColors == 0) return;

            var palette = RetrievePalette(currentImage, swatchWidth, swatchHeight, numberOfColors);
            ImportedPaletteView.Colors = palette;

            paletteDictionary.Clear();
            Warning = null;
            for (int i = 0; i < palette.Count; i++)
            {
                byte value = (byte)((i / (float)(numberOfColors - 1)) * 255);
                if (paletteDictionary.ContainsKey(palette[i])) Warning = "There are duplicate colors in the palette.";
                else paletteDictionary[palette[i]] = BitmapHelper.CreateColor(value, value, value, 255);
            }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Warning"));
            IndexedPaletteView.Colors = palette.Select(x => paletteDictionary[x]).ToList();
        }

        private ImageSource Palettize(BitmapSource source)
        {
            if (source == null) return null;

            WriteableBitmap bitmap = new WriteableBitmap(source);

            var pixels = bitmap.CopyPixels();
            Warning = null;
            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    var color = pixels[i, j];
                    if (color.A == 0) continue;

                    var newColor = color;
                    newColor.A = 255;

                    if (paletteDictionary.ContainsKey(newColor)) newColor = paletteDictionary[newColor];
                    else
                    {
                        Warning = "Colors not in the palette found. Setting to black.";
                        newColor = Colors.Black;
                    }
                    newColor.A = color.A;
                    pixels[i, j] = newColor;
                }
            }

            bitmap.WritePixels(pixels);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Warning"));
            return bitmap;
        }

        private void OnPalettizeClick(object sender, RoutedEventArgs e)
        {
            if (currentImage == null) return;
            (PreviewImage.Template.FindName("Image", PreviewImage) as Image).Source = Palettize(currentImage);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            RenderOptions.SetBitmapScalingMode(SourceImage.Template.FindName("Image", SourceImage) as Image, BitmapScalingMode.NearestNeighbor);
            RenderOptions.SetBitmapScalingMode(PreviewImage.Template.FindName("Image", PreviewImage) as Image, BitmapScalingMode.NearestNeighbor);
        }

        private void OnSaveClick(object sender, RoutedEventArgs e)
        {
            var source = (PreviewImage.Template.FindName("Image", PreviewImage) as Image).Source as BitmapSource;
            if (source == null) return;

            string fileName;
            if ((fileName = source.SaveAsPng(lastFile)) == null) return;

            (SourceImage.Template.FindName("Image", SourceImage) as Image).Source = source;
            currentImage = source;
            lastFile = fileName;
        }
    }

    public class PaletteColor
    {
        public SolidColorBrush Color { get; private set; }
        public PaletteColor(Color c) { Color = new SolidColorBrush(c); }
    }

    public class IsNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object param, CultureInfo culture)
        {
            return value == null;
        }

        public object ConvertBack(object value, Type targetType, object param, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsValidIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object param, CultureInfo culture)
        {
            int o;
            return int.TryParse(value as string, out o);
        }

        public object ConvertBack(object value, Type targetType, object param, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new SolidColorBrush((Color)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ItemCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((IEnumerable)value)?.Cast<object>().Count() ?? 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
